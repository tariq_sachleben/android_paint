package tsachleben.i5.model;

import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tsachleben.i5.viewobject.IDrawable;

/**
 * Primary Model class containing data and handling transitions of that data
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */

public class PaintCanvasData {

    private DrawableBuilder currentBuilder;
    private Paint mPaint;
    private ArrayList<IDrawable> items;
    private ArrayList<View> views;

    public PaintCanvasData() {
        views = new ArrayList<>();

        mPaint = new Paint();
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.FILL);

        clearCanvas();
        startCurve();
    }

    public void startCurve() {
        //mSelector = ShapeSelectorState.CURVE;
        currentBuilder = DrawableBuilder.startCurve(getPaint());
    }

    public void startLine() {
        //mSelector = ShapeSelectorState.LINE;
        currentBuilder = DrawableBuilder.startLine(getPaint());
    }

    public void startRectangle() {
        //mSelector = ShapeSelectorState.RECTANGLE;
        currentBuilder = DrawableBuilder.startRectangle(getPaint());
    }

    public void startOval() {
        //mSelector = ShapeSelectorState.OVAL;
        currentBuilder = DrawableBuilder.startOval(getPaint());
    }

    public void cancelShape() {
        currentBuilder.reset();
    }

    public void finishShape() {
        addItem(currentBuilder.build());
        currentBuilder.reset();
    }

    public void touchEvent(float x, float y) {
        currentBuilder.feedPoint(x, y);
    }

    public void undo() {
        if (items.size() < 1) {
            return;
        }
        items.remove(items.size() - 1);
        notifyViews();
    }

    public void clearCanvas() {
        items = new ArrayList<>();
        notifyViews();
    }

    public void updateColor(int color) {
        mPaint.setColor(color);
        currentBuilder.reset(mPaint);
    }

    public void updateBrushSize(float r) {
        mPaint.setStrokeWidth(r);
        currentBuilder.reset(mPaint);
    }

    public float getBrushSize() {
        return mPaint.getStrokeWidth();
    }

    public void registerView(View v) {
        views.add(v);
    }

    public List<IDrawable> getDrawables() {
        return Collections.unmodifiableList(items);
    }

    private Paint getPaint() {
        return new Paint(mPaint);
    }

    private void addItem(IDrawable d) {
        items.add(d);
        notifyViews();
    }

    private void notifyViews() {
        for (View v : views) {
            v.invalidate();
        }
    }
}
