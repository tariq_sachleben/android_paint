package tsachleben.i5.model;

import android.graphics.Paint;
import android.graphics.RectF;

import java.util.ArrayList;

import tsachleben.i5.viewobject.IDrawable;
import tsachleben.i5.viewobject.Line;
import tsachleben.i5.viewobject.Oval;
import tsachleben.i5.viewobject.PointCurve;
import tsachleben.i5.viewobject.Rectangle;

/**
 * Builder type(s)
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */

abstract class DrawableBuilder {

    // Public Interface for getting Factories
    static DrawableBuilder startRectangle(Paint p) {
        return new BoxBoundBuilder.RectangleBuilder(p);
    }

    static DrawableBuilder startOval(Paint p) {
        return new BoxBoundBuilder.OvalBuilder(p);
    }

    static DrawableBuilder startLine(Paint p) {
        return new LineBuilder(p);
    }

    static DrawableBuilder startCurve(Paint p) {
        return new CurveBuilder(p);
    }

    // Factory Interface
    protected Paint mPaint;

    protected DrawableBuilder(Paint p) {
        mPaint = new Paint(p);
    }

    public abstract IDrawable build();
    public abstract void reset();
    public abstract void feedPoint(float x, float y);

    // Convenience method
    public void reset(Paint p) {
        reset();
        mPaint = new Paint(p);
    }

    // Factory types
    private static abstract class BoxBoundBuilder extends DrawableBuilder {

        private float[] first;
        private float[] last;

        BoxBoundBuilder(Paint p) {
            super(p);
        }

        @Override
        public void reset() {
            first = null;
            last = null;
        }

        @Override
        public void feedPoint(float x, float y) {
            if (first == null) {
                first = new float[] {x, y};
            } else {
                last = new float[] {x, y};
            }
        }

        @Override
        public IDrawable build() {
            if (first == null || last == null)
                throw new IllegalStateException("Not enough points entered");

            float left, top, right, bottom;
            if (first[0] < last[0]) {
                left = first[0];
                right = last[0];
            } else {
                left = last[0];
                right = first[0];
            }
            if (first[1] < last[1]) {
                top = first[1];
                bottom = last[1];
            } else {
                top = last[1];
                bottom = first[1];
            }

            RectF r = new RectF(left, top, right, bottom);
            return buildBoxBound(r, mPaint);
        }

        protected abstract IDrawable buildBoxBound(RectF r, Paint p);

        private static class RectangleBuilder extends BoxBoundBuilder {

            RectangleBuilder(Paint p) {
                super(p);
            }

            @Override
            protected IDrawable buildBoxBound(RectF r, Paint p) {
                return new Rectangle(r, p);
            }

        }

        private static class OvalBuilder extends BoxBoundBuilder {

            OvalBuilder(Paint p) {
                super(p);
            }

            @Override
            protected IDrawable buildBoxBound(RectF r, Paint p) {
                return new Oval(r, p);
            }

        }
    }

    private static class LineBuilder extends DrawableBuilder {

        private float[] first;
        private float[] last;

        LineBuilder(Paint p) {
            super(p);
        }

        @Override
        public void reset() {
            first = null;
            last = null;
        }

        @Override
        public void feedPoint(float x, float y) {
            if (first == null) {
                first = new float[] {x, y};
            } else {
                last = new float[] {x, y};
            }
        }

        @Override
        public IDrawable build() {
            return new Line(first, last, mPaint);
        }

    }

    private static class CurveBuilder extends DrawableBuilder {

        private ArrayList<Float> mPoints;

        CurveBuilder(Paint p) {
            super(p);
            this.reset();
        }

        @Override
        public void reset() {
            mPoints = new ArrayList<>();
        }

        @Override
        public void feedPoint(float x, float y) {
            mPoints.add(x);
            mPoints.add(y);
        }

        @Override
        public IDrawable build() {
            return new PointCurve(mPoints, mPaint);
        }

    }

}
