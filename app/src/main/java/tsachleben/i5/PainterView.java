package tsachleben.i5;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;

import tsachleben.i5.model.PaintCanvasData;
import tsachleben.i5.viewobject.IDrawable;

/**
 * Custom View that draws internal drawables
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */

public class PainterView extends View {

    private PaintCanvasData mData;

    public PainterView(MainActivity activity) {
        super(activity);
        setWillNotDraw(false);

        mData = activity.requestPaintCanvasData();
        mData.registerView(this);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.WHITE);
        for (IDrawable i : mData.getDrawables()) {
            i.onDraw(canvas);
        }
    }

}
