package tsachleben.i5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import tsachleben.i5.controller.PaintTouchListener;
import tsachleben.i5.model.PaintCanvasData;

/**
 * Base Class for starting the android application and configuring connections between internal
 * elements
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */
public class MainActivity extends AppCompatActivity {

    private PaintCanvasData mData;
    private PainterView mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData = new PaintCanvasData();
        mView = new PainterView(this);
        mView.setOnTouchListener(new PaintTouchListener(this));

        mData.updateBrushSize(3.2f);
        setContentView(mView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.selector_curve:
                mData.startCurve();
                return true;
            case R.id.selector_line:
                mData.startLine();
                return true;
            case R.id.selector_rectangle:
                mData.startRectangle();
                return true;
            case R.id.selector_oval:
                mData.startOval();
                return true;
            case R.id.action_undo:
                mData.undo();
                return true;
            case R.id.action_erase:
                mData.clearCanvas();
                return true;
            case R.id.action_settings:
                openSettingsDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public PaintCanvasData requestPaintCanvasData() {
        return mData;
    }

    private void openSettingsDialog() {
        new SettingsDialog(this).show();
    }

}
