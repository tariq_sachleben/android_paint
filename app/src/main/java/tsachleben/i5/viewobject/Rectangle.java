package tsachleben.i5.viewobject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by tsachleben on 10/10/17.
 */

public class Rectangle extends BoxBoundDrawable {

    public Rectangle(RectF r, Paint p) {
        super(r, p);
    }

    @Override
    protected void draw(Canvas c, RectF r, Paint p) {
        c.drawRect(r, p);
    }

}
