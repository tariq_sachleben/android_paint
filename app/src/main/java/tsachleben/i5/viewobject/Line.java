package tsachleben.i5.viewobject;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by tsachleben on 10/17/17.
 */

public class Line implements IDrawable {

    private float[] first;
    private float[] last;
    private Paint mPaint;

    public Line(float[] first, float[] last, Paint p) {

        if (first.length != 2 || last.length != 2)
            throw new IllegalArgumentException("Both Arrays must be arity 2");

        this.first = first;
        this.last = last;
        mPaint = new Paint(p);
        mPaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    public void onDraw(Canvas c) {
        c.drawLine(first[0], first[1], last[0], last[1], mPaint);
    }

}
