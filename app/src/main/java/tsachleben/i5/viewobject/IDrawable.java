package tsachleben.i5.viewobject;

import android.graphics.Canvas;

/**
 * Created by tsachleben on 10/10/17.
 */

public interface IDrawable {
    void onDraw(Canvas c);
}
