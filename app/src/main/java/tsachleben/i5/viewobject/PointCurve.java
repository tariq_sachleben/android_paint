package tsachleben.i5.viewobject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import java.util.List;

/**
 * Created by tsachleben on 10/10/17.
 */

public class PointCurve implements IDrawable {

    private Path mPath;
    private Paint mPaint;

    public PointCurve(List<Float> points, Paint p) {

        if (points.size() < 4 || points.size() % 2 != 0)
            throw new IllegalArgumentException();

        mPath = new Path();
        mPath.setLastPoint(points.get(0), points.get(1));
        for (int i = 0; i < points.size(); i+=2)
            mPath.lineTo(points.get(i), points.get(i + 1));

        mPaint = new Paint(p);
        mPaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    public void onDraw(Canvas c) {
        c.drawPath(mPath, mPaint);
    }

}
