package tsachleben.i5.viewobject;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import tsachleben.i5.viewobject.IDrawable;

/**
 * Created by tsachleben on 10/16/17.
 */

abstract class BoxBoundDrawable implements IDrawable {

    private RectF mRect;
    private Paint mPaint;

    protected BoxBoundDrawable(RectF r, Paint p) {
        mRect = new RectF(r);
        mPaint = new Paint(p);
    }

    @Override
    public void onDraw(Canvas c) {
        this.draw(c, mRect, mPaint);
    }

    protected abstract void draw(Canvas c, RectF r, Paint p);

}
