package tsachleben.i5;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import tsachleben.i5.MainActivity;
import tsachleben.i5.R;
import tsachleben.i5.model.PaintCanvasData;

/**
 * Controller Element for the Settings Dialogue
 * Similar in form to a Fragment but not
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */

public class SettingsDialog extends Dialog implements Spinner.OnItemSelectedListener {

    private PaintCanvasData mData;
    private Spinner colorSpinner;
    private int spinnerSelection;
    private EditText customColorText;
    private SeekBar brushSizeSeek;

    public SettingsDialog(MainActivity activity) {
        super(activity);
        mData = activity.requestPaintCanvasData();
    }

    @Override
    public void onCreate(Bundle savedInstanaceState) {
        setContentView(R.layout.dialog_settings);

        Button buttonOk = findViewById(R.id.buttonOK);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mData.updateColor(getColor());
                } catch (ColorParseException e) {
                    Toast.makeText(getContext(), "Illegal Color Specification", Toast.LENGTH_SHORT).show();
                }

                mData.updateBrushSize(brushSizeSeek.getProgress() / 10f);
                dismiss();
            }
        });

        Button buttonCancel = findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        colorSpinner = findViewById(R.id.colorSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.color_selector_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(adapter);
        colorSpinner.setOnItemSelectedListener(this);

        brushSizeSeek = findViewById(R.id.brushSizeSeek);
        brushSizeSeek.setMax(100);
        brushSizeSeek.setProgress((int) mData.getBrushSize() * 10);

        customColorText = findViewById(R.id.customColorText);
    }

    //@Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinnerSelection = position;
        CharSequence selected = (CharSequence) parent.getItemAtPosition(spinnerSelection);
        boolean newState = selected.equals("Custom");
        customColorText.setFocusable(newState);
        customColorText.setFocusableInTouchMode(newState);
        customColorText.setClickable(newState);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do Nothing
    }

    private int getColor() throws ColorParseException {
        switch (colorSpinner.getItemAtPosition(spinnerSelection).toString()) {
            case "Red":
                return Color.RED;
            case "Orange":
                return Color.argb(255, 255, 152, 0);
            case "Yellow":
                return Color.YELLOW;
            case "Green":
                return Color.GREEN;
            case "Blue":
                return Color.BLUE;
            case "Purple":
                return Color.argb(255, 106, 27, 154);
            case "Black":
                return Color.BLACK;
            case "Custom":
                try {
                    return Color.parseColor(customColorText.getText().toString());
                } catch (IllegalArgumentException e) {
                    throw new ColorParseException(e);
                }
            default:
                throw new UnknownError("Unreachable");
        }
    }

    private static class ColorParseException extends Exception {
        ColorParseException(Exception cause) {
            super(cause);
        }
    }
}
