package tsachleben.i5.controller;

import android.view.MotionEvent;
import android.view.View;

import tsachleben.i5.MainActivity;
import tsachleben.i5.model.PaintCanvasData;

/**
 * Touch Event Listener for Paint Canvas Events
 *
 * Written for Northeastern University IS 4300 Individual Homework 5
 * @author tsachleben
 */

public class PaintTouchListener implements View.OnTouchListener {

    private PaintCanvasData mData;

    public PaintTouchListener(MainActivity activity) {
        mData = activity.requestPaintCanvasData();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                int history = event.getHistorySize();
                for (int i = 0; i < history; i++)
                    mData.touchEvent(event.getHistoricalX(i), event.getHistoricalY(i));
                mData.touchEvent(event.getX(), event.getY());
                return true;

            case MotionEvent.ACTION_UP:
                mData.touchEvent(event.getX(), event.getY());
                mData.finishShape();
                return true;

            case MotionEvent.ACTION_CANCEL:
                mData.cancelShape();
                return true;

            default:
                return false;
        }
    }

}
